from bs4 import BeautifulSoup
import datetime
from concurrent.futures import ThreadPoolExecutor
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException        
import re
from threading import Thread
import credentials
import psycopg2
import random
from fake_useragent import UserAgent

# import logging
# import inspect
# print(inspect.getsource(logging))

##connect to postgresql
#try:
#    login = "dbname=%s user=%s host=%s password=%s" % (credentials.login['dbname'],credentials.login['user'],credentials.login['host'],credentials.login['password'])
#    postgresqlconn = psycopg2.connect(login)
#except Exception as e:
#    print("cannot connect to Google postgresSQL")
#    print(e)
#cur = postgresqlconn.cursor()# Google Location Setting

domain = 'https://www.google.com'

# Speed Settings
threads = 2

#category
category = 'serps_tracker_manual'

unix = int(time.time())
now = datetime.datetime.now()
date = now.strftime("%Y-%m-%d")
keyword = [line.rstrip('\n') for line in open('kw_bathroom.txt')]
keywords = [str.replace(x,' ','+') for x in keyword]

def check_exists_by_xpath(xpath):
    try:
        webdriver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def runQuery(query):
    try:
        connect_text = "dbname=%s user=%s host=%s password=%s" % (credentials.login['dbname'],credentials.login['user'],credentials.login['host'],credentials.login['password'])
        con = psycopg2.connect(connect_text)
    except psycopg2.IntegrityError, Exception:
        postgresqlconn.rollback()
        print Exception        
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    print 'commit'
    return cur
    con.close()

def ranks(i):
    chrome_options = Options()  
    chrome_options.add_argument("--headless")  
    #options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    chrome_options = webdriver.ChromeOptions()
    #ua = UserAgent()    
    #userAgent = ua.random
    #print(userAgent) 
    #user_agent = '''--user-agent="%s"'''% (userAgent)
    #chrome_options.add_argument(user_agent)
    chrome_options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")

    driver = webdriver.Chrome(executable_path='/Users/jason/Documents/repo/rank_tracker/chromedriver', chrome_options=chrome_options)
    url = domain + '#q=' + i + '&num=50'
    driver.get(url)    
    time.sleep(5)
    serps_element = None
    while not serps_element:
        try:
            serps_element = driver.find_element_by_xpath('//*[@valign="top"]')
            print('SERPs page found')
        except NoSuchElementException:
            print('SERPS page not found, please fill out CAPTCHA')
            time.sleep(10)

    soup = BeautifulSoup(driver.page_source, "html.parser")
    print("Opening this page " + domain+ '#q='+i)

    try:
        results = soup.findAll('div',attrs={'class':'g'})
        print(len(results))
        for a,b in enumerate(results):
            soup = b
            header = soup.find('h3')
            result = a + 1

            link = soup.find('a')
            url = link['href']
            url = re.sub(r'/url\?q=', '',str(url)) 
            url = re.sub(r'&sa=.*', '',str(url))

            result_type = "Standard Serp"
            global category

            i_keyword = i.replace('+',' ')

            #qry = """INSERT INTO rank (_date, unix, keyword, _rank, _type, url, _category) VALUES ('%s'::DATE, %s, '%s', %s, '%s'::TEXT, '%s'::TEXT, '%s'::TEXT)"""
            qry = """INSERT INTO deepdive (_date, unix, keyword, _rank, _type, url, _category) VALUES ('%s'::DATE, %s, '%s', %s, '%s'::TEXT, '%s'::TEXT, '%s'::TEXT)"""
            query = qry % (date, unix, i_keyword, result, result_type, url, category)
            print(date, unix, i_keyword, result, result_type, url, category)
            runQuery(query) 


    except Exception as e:
        print(e)
    driver.quit()

futures = []
with ThreadPoolExecutor(max_workers=threads) as ex:
    for keyword in keywords:
        futures.append(ex.submit(ranks,keyword))