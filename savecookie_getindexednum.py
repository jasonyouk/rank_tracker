#from bs4 import BeautifulSoup
import datetime
from concurrent.futures import ThreadPoolExecutor
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException        
import re
from threading import Thread
import credentials
#import psycopg2
import random
#from fake_useragent import UserAgent
from lxml import html, etree
import csv

# import logging
# import inspect
# print(inspect.getsource(logging))

##connect to postgresql
#try:
#    login = "dbname=%s user=%s host=%s password=%s" % (credentials.login['dbname'],credentials.login['user'],credentials.login['host'],credentials.login['password'])
#    postgresqlconn = psycopg2.connect(login)
#except Exception as e:
#    print("cannot connect to Google postgresSQL")
#    print(e)
#cur = postgresqlconn.cursor()# Google Location Setting

domain = 'https://www.google.com'

# Speed Settings
threads = 2

#category
category = 'serps_tracker_manual'

unix = int(time.time())
now = datetime.datetime.now()
date = now.strftime("%Y-%m-%d")
#keyword = [line.rstrip('\n') for line in open('similarity.txt')]
#keywords = [str.replace(x,' ','+') for x in keyword]
#keywords = [str.replace(x,' ','+') for x in keyword]
with open('/Users/jason/Desktop/subdomian.txt', 'r') as f:
    start_urls = ['site:'+url.strip()+'.houzz.com' for url in f.readlines()]
    start_urls = set(start_urls)
    #start_urls = 'http://google.com/search#q=site:houzz.com'
output_csv_filename = '/Users/jason/Desktop/subdomain_results.csv'    

print(start_urls)

def check_exists_by_xpath(xpath):
    try:
        webdriver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

#def runQuery(query):
#     try:
#         connect_text = "dbname=%s user=%s host=%s password=%s" % (credentials.login['dbname'],credentials.login['user'],credentials.login['host'],credentials.login['password'])
#         con = psycopg2.connect(connect_text)
#     except psycopg2.IntegrityError, Exception:
#         postgresqlconn.rollback()
#         print Exception        
#     cur = con.cursor()
#     cur.execute(query)
#     con.commit()
#     print 'commit'
#     return cur
#     con.close()

#write results to csv
def to_csv(url, results):
    #print('test2')
    #with open(output_csv_filename, 'a', source ='utf8') as csv_file:
    with open(output_csv_filename, 'a') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"')
        #redirect_status = (lambda x: True if x != redirect_url else False)(original_url)
        results = ', '.join(str(v) for v in results)
        url = url.replace('https://www.google.com#q=site:','').replace('&num=50','')
        csv_writer.writerow([url, results])


chrome_options = Options()  
#chrome_options.add_argument("--headless")  
#options.add_argument('--disable-gpu')  # Last I checked this was necessary.
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("user-data-dir=selenium") 

#ua = UserAgent()    
#userAgent = ua.random
#print(userAgent) 
#user_agent = '''--user-agent="%s"'''% (userAgent)
#chrome_options.add_argument(user_agent)
chrome_options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")   

def ranks(i):
    driver = webdriver.Chrome(executable_path='/Users/jason/Documents/repo/rank_tracker/chromedriver', chrome_options=chrome_options)
    #https://www.google.com/search?q=site:info.houzz.com
    url = domain + '#q=' + i + '&num=50'
    driver.get(url)    
    #time.sleep(5)
    serps_element = None
    while not serps_element:
        try:
            serps_element = driver.find_element_by_xpath('//*[@valign="top"]')
            print('SERPs page found')
        except NoSuchElementException:
            print('SERPS page not found, please fill out CAPTCHA')
            time.sleep(10)

    #soup = BeautifulSoup(driver.page_source, "html.parser")
    tree = html.fromstring(driver.page_source)

    print("Opening this page " + domain+ '#q='+i)

    try:
        #results = soup.findAll('div',attrs={'class':'g'})
        #results =  soup.find("div", {"id": "resultStats"})
        results =  tree.xpath('//div[@id="resultStats"]/text()')       
        if results is not None:
            print(url, results)
            to_csv(url, results)
        else:
            rint(url, 'NONE BUDDDY!')
            to_csv(url, None)
            #runQuery(query)         
    except Exception as e:
        print(e)
    driver.quit()
    #driver.close()

# futures = []
# with ThreadPoolExecutor(max_workers=threads) as ex:
#     for keyword in keywords:
#         futures.append(ex.submit(ranks,keyword))

rows = 0
for url in start_urls:
    rows += 1
    ranks(url)
    print(rows)
